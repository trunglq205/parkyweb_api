﻿namespace ParkyWeb
{
    public static class SD
    {
        public static string APIBaseUrl = "https://localhost:44348/";
        public static string NationalParkAPIPath = APIBaseUrl + "api/v1/NationalPark/";
        public static string TrailAPIPath = APIBaseUrl + "api/v1/Trail/";
        public static string AccountAPIPath = APIBaseUrl + "api/v1/User/";


    }
}

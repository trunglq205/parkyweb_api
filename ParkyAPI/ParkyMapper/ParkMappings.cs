﻿using AutoMapper;
using ParkyAPI.Models;
using ParkyAPI.Models.DTO;

namespace ParkyAPI.ParkyMapper
{
    public class ParkMappings : Profile
    {
        public ParkMappings()
        {
            CreateMap<NationalPark, NationalParkDTO>().ReverseMap();
            CreateMap<Trail, TrailDTO>().ReverseMap();
            CreateMap<Trail, TrailCreateDTO>().ReverseMap();
            CreateMap<Trail, TrailUpdateDTO>().ReverseMap();


        }
    }
}
